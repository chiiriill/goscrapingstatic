package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/PuerkitoBio/goquery"
)

func main() {
	file_, err := os.Create("data.csv")
	if err != nil {
		panic(err)
	}
	defer file_.Close()

	writer := csv.NewWriter(file_)
	defer writer.Flush()
	for page := 1; page < 38; page++ {
		titles, err := GetData(fmt.Sprintf("https://biggeek.ru/catalog/apple?page=%d", page))
		if err != nil {
			log.Println(err)
		}

		for _, el := range titles {
			writer.Write(el)
		}
		fmt.Printf("[INFO]Page %d compleate!\n", page)
	}
}

func GetData(url string) ([][]string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return nil, err
	}

	var res [][]string
	doc.Find(".catalog-card").Each(func(i int, s *goquery.Selection) {
		var product []string
		label := s.Find(".catalog-card__title")
		price := s.Find(".cart-modal-count")
		link, _ := label.Attr("href")
		product = append(product, label.Text(), price.Text(), link)
		res = append(res, product)
	})

	return res, nil
}
